---
title: Formation « Débuter en Python »
author: Arnaud Mounier
numbersections: true
---

# Premiers pas

## Blocs d'instruction et identation

[Instructions et blocs](https://python.sdv.univ-paris-diderot.fr/01_introduction/)

## Variables : chaîne de charatères et nombres

[Variables](https://python.sdv.univ-paris-diderot.fr/02_variables/)

+ Exercices

## Affichage

[Display](https://python.sdv.univ-paris-diderot.fr/03_affichage/)

+ Exercices

# Strutures de données
## Les listes et les tuples

[Listes](https://python.sdv.univ-paris-diderot.fr/04_listes/)

+ Exercices

[Tuples](http://introtopython.org/lists_tuples.html)

+ Exercices

## Les ensembles

À vous de jouer...

## Les dictionnaires

[Dictionnaires et tuples](https://python.sdv.univ-paris-diderot.fr/13_dictionnaires_tuples/)

+ Exercice 13.3.1

# Rupture du flux d'instructions
## Les tests

[Tests](https://python.sdv.univ-paris-diderot.fr/06_tests/)

+ Excercices

## Les boucles et les comparaisons

[Boucles et comparaisons](https://python.sdv.univ-paris-diderot.fr/05_boucles_comparaisons/)

+ Exercices

## S'échaper d'une boucle

[break et continue](https://docs.python.org/fr/3.6/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops)
[pass](https://docs.python.org/fr/3.6/tutorial/controlflow.html#pass-statements)

## La fonction énumérer (enumarate)
[La fonction enumerate](https://docs.python.org/fr/3.6/library/functions.html#enumerate)

## Comprehension de listes

[Comprehension de listes](https://python.sdv.univ-paris-diderot.fr/21_remarques_complementaires/)

+ Exercices

# Les fonctions et les modules

- [Fonctions](https://python.sdv.univ-paris-diderot.fr/09_fonctions/)
- [Modules](https://python.sdv.univ-paris-diderot.fr/14_creation_modules/)

# Anticiper les erreurs internes

[Try ou except](https://python.sdv.univ-paris-diderot.fr/21_remarques_complementaires/)

# Gestion des fichiers
## Les fichiers génériques
[Fichier](https://python.sdv.univ-paris-diderot.fr/07_fichiers/)

## Les fichiers CSV
[Fichiers CSV](https://docs.python.org/fr/3.7/library/csv.html)

# Challenges

[Rosalind](http://rosalind.info/problems/locations/)

[PythonChallenge](http://www.pythonchallenge.com/)
